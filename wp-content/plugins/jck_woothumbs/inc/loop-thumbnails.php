<?php global $jck_wt; ?>

<?php if(!empty($images)) { ?>

    <div class="<?php echo $this->slug; ?>-thumbnails-wrap <?php echo $this->slug; ?>-thumbnails-wrap--<?php echo $jck_wt['navigationType']; ?>">
        	
    	<div class="<?php echo $this->slug; ?>-thumbnails">
    	
    	    <?php $image_count = count($images); ?>
    	    
    	    <?php if( $image_count > 1 ) { ?>
    	
        	    <?php $i = 0; foreach($images as $image): ?>
            		
            		<div class="<?php echo $this->slug; ?>-thumbnails__slide <?php if($i == 0) { ?><?php echo $this->slug; ?>-thumbnails__slide--active<?php } ?>" data-index="<?php echo $i; ?>">
            		
            			<img class="<?php echo $this->slug; ?>-thumbnails__image" src="<?php echo $image['thumb'][0]; ?>" title="<?php echo $image['title']; ?>" alt="<?php echo $image['alt']; ?>">
            		
            		</div>
            		
            	<?php $i++; endforeach; ?>
            	
            	<?php 
                	
                // pad out thumbnails if there are less than the number
                // which are meant to be shown.
                
                if( $image_count < $jck_wt['thumbnailCount'] ) {
                	
                	$empty_count = $jck_wt['thumbnailCount'] - $image_count;
                	$i = 0;
                	
                	while( $i < $empty_count ) {
                    	
                    	echo "<div></div>";            	
                    	$i++;
                    	
                	}
                	
            	} 
            	
            	?>
        	
        	<?php } ?>
    	
    	</div>
    
    </div>

<?php } ?>